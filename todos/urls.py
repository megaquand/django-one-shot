from django.urls import path
from todos.views import show_TodoList

urlpatterns = [
    path("", show_TodoList, name="todo_list_list"),
]
