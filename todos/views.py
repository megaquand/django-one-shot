from django.shortcuts import render
from todos.models import TodoList


def show_TodoList(request):
    Todos = TodoList.objects.all()
    context = {
        "TodoList": Todos,
    }
    return render(request, "list.html", context)
